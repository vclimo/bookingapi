from django.contrib import admin
from .models import Booking, Promotions, Payments, Source

# Register your models here.

admin.site.register(Booking)
admin.site.register(Promotions)
admin.site.register(Payments)
admin.site.register(Source)
