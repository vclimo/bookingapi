from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Booking(models.Model):

    class Status(models.IntegerChoices):
        PENDING = 0
        ACTIVE = 1
        COMPLETED = 2
        CANCELED = 3

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    departure_time = models.DateTimeField(auto_created=False)
    pick_up = models.CharField(max_length=100)
    destination = models.CharField(max_length=100)
    status = models.IntegerField(choices=Status.choices)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)


class Promotions(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    booking = models.ForeignKey(Booking, on_delete=models.CASCADE)
    discount = models.DecimalField(max_digits=10, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)


class Source(models.Model):

    class Status(models.IntegerChoices):
        PENDING = 0
        ACTIVE = 1
        SUSPENDED = 2

    name = models.CharField(max_length=30)
    status = models.IntegerField(choices=Status.choices)
    created_at = models.DateTimeField(auto_now_add=True)


class Payments(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    booking = models.ForeignKey(Booking, on_delete=models.CASCADE)
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)
