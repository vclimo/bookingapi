from rest_framework import routers
from .views import BookingViewSet

router = routers.DefaultRouter()
router.register('v1/bookings', BookingViewSet)

urlpatterns = router.urls
