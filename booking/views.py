from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404
from .serializer import BookingSerializer, UserSerializer
from .models import Booking
from rest_framework import (
    viewsets, generics, views, status, permissions, exceptions
)
from rest_framework.response import Response
from rest_framework.authtoken.models import Token


class BookingViewSet(viewsets.ModelViewSet):
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        if self.action == 'list':
            return self.queryset.filter(user=self.request.user)
        return self.queryset


class UserCreate(generics.CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserSerializer


class LoginView(views.APIView):
    permission_classes = ()

    @staticmethod
    def post(self, request,):
        username = request.data.get("username")
        password = request.data.get("password")

        user = authenticate(username=username, password=password)

        if user:
            token, _ = Token.objects.get_or_create(user=user)
            return Response({"token": token.key}, status=status.HTTP_200_OK)
        else:
            return Response({"error": "Wrong Credentials"},
                            status=status.HTTP_400_BAD_REQUEST)
